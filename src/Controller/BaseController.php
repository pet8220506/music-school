<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BaseController extends AbstractController
{
    /**
     * Name or instance of entity class
     * @var string|object
     */
    public string|object $entity;

    protected EntityManagerInterface $em;

    /**
     * @param int $value
     * @param string $field
     *
     * @return object|null
     * @throws NotFoundHttpException
     */
    protected function findEntity(int $value, string $field = 'id'): ?object
    {
        $entity = $this->em->getRepository($this->entity)->find([$field => $value]);

        if ($entity === null) {
            throw new NotFoundHttpException();
        }

        return $entity;
    }

}