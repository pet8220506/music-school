<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends BaseController
{
    public function __construct(protected EntityManagerInterface $em)
    {
        $this->entity = Product::class;
    }

    #[Route('/product', name: 'product.index')]
    /**
     * @return Response
     */
    public function index(): Response
    {
        $products = $this->em->getRepository(Product::class)->findAll();

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products,
        ]);
    }

    #[Route('/product/{id}/edit', name: 'product.edit', requirements: ['id' => "\d+"], methods: "GET")]
    /**
     * @param int $id
     * @param Request $request
     * @var Product|null $entity
     * @return Response
     */
    public function edit(int $id, Request $request): Response
    {
        $entity = $this->findEntity($id);
        $form = $entity->getService()->getPreparedForm($this->createFormBuilder($entity));

        return $this->render('product/edit.html.twig', [
            'controller_name' => 'ProductController',
            'form' => $form->createView()
        ]);
    }

    #[Route('/product/{id}/update', name: 'product.update', requirements: ['id' => "\d+"], methods: "POST|PUT|PATCH")]
    /**
     * @param Request $request
     * @param $id
     * @var Product|null $entity
     * @return Response
     */
    public function update(Request $request, $id): Response
    {
        $entity = $this->findEntity($id);
        $form = $entity->getService()->getPreparedForm($this->createFormBuilder($entity));

        $form->handleRequest($request);

        $entity->getService()->save($this->em, $form);

        return $this->redirectToRoute('product.index');
    }


}
