<?php

namespace App\Service;

class BaseService
{
    protected object $entity;

    public function __construct(object $entity)
    {
        $this->entity = $entity;
    }
}