<?php

namespace App\Service;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @property Product $entity
 */
class ProductService extends BaseService
{
    public function getPreparedForm(FormBuilderInterface $builder)
    {
        return $builder->add('title', TextType::class)
            ->getForm();
    }

    public function save(EntityManagerInterface $em, $form) : void
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($form->getData());
            $em->flush();
        }
    }
}